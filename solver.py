import numpy as np
import copy
from scipy.integrate import ode
from variables import *

class Solver:

  def __init__(self, differential_processes):
    self.differential_processes = differential_processes
    self.__initialize_ode_solver()

  def __initialize_ode_solver(self):
    self.ode_solver = ode(f=self.__derivatives, jac=self.__jacobian)
    self.ode_solver.set_integrator("vode", method="bdf")

  def __derivatives(self, t, y):
    derivatives = empty_variables()
    for differential_process in self.differential_processes:
      current_derivatives = differential_process.get_derivatives(y, self.constants)
      derivatives += current_derivatives
    return derivatives

  def __jacobian(self, t, y):
    jacobian = empty_jacobian()
    for differential_process in self.differential_processes:
      current_jacobian = differential_process.get_jacobian(y, self.constants)
      jacobian += current_jacobian
    return jacobian

  def solve(self, time, y0, constants):
    t0 = time[0]
    self.ode_solver.set_initial_value(y0, t0)
    self.constants = constants
    values = self.__solve_all_timesteps(time)
    return values
  
  def __solve_all_timesteps(self, time):
    num_steps = len(time)
    values = np.zeros( (num_steps, NUM_VARIABLES) )
    steps = range(1, num_steps)
    time = time[1:]
    for (current_step, current_time) in zip(steps, time):
      current_values = self.__integrate_step(current_time)
      values[current_step, :] = current_values
    return values

  def __integrate_step(self, current_time):
    self.ode_solver.integrate(current_time)
    if not self.ode_solver.successful():
      raise RuntimeError('The solver encountered a problem at ' + str(current_time))
    return self.ode_solver.y
