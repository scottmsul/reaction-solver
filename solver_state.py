class SolverState:

  def __init__(self, state_vector, variable_index_map, temperature, tracked_density):
    self.state_vector = state_vector
    self.variable_index_map = variable_index_map
    self.temperature = temperature
    self.tracked_density = tracked_density

  def get_chemical_fraction(self, chemical_name):
    chemical_index = self.variable_index_map.get_index_from_name(chemical_name)
    chemical_fraction = self.state_vector[chemical_index]
    return chemical_fraction

  def get_temperature(self):
    return self.temperature

  def get_tracked_density(self):
    return self.tracked_density
