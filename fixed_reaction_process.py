import copy
import numpy as np
from variables import *

class FixedReactionProcess:

  def __init__(self, reaction):
    self.reactants_dict = reaction['reactants']
    self.products_dict = reaction['products']
    self.chemicals_dict = reaction['chemicals']

    (reactants, reactant_coefficients) = self.__initialize_chemical_set(reaction['reactants'])
    self.reactants = reactants
    self.num_reactants = len(reactants)
    self.reactant_coefficients = reactant_coefficients

    (products, product_coefficients) = self.__initialize_chemical_set(reaction['products'])
    self.products = products
    self.num_products = len(products)
    self.product_coefficients = product_coefficients

    (chemicals, chemical_coefficients) = self.__initialize_chemical_set(reaction['chemicals'])
    self.chemicals = chemicals
    self.num_chemicals = len(chemicals)
    self.chemical_coefficients = chemical_coefficients

    self.diagram = reaction['diagram']
    self.rate_function = reaction['rate_function']

  def __initialize_chemical_set(self, name_map):
    indices = []
    coefficients = []
    for chemical, coefficient in name_map.iteritems():
      index = CHEMICAL_INDEX_MAP[chemical]
      indices.append(index)
      coefficients.append(coefficient)
    return (indices, coefficients)

  def get_derivatives(self, y, constants):
    self.__initialize_values(y, constants)
    density_product = self.__compute_density_product()
    fractions_product = self.__compute_fractions_product()
    change = self.rate * density_product * fractions_product

    derivatives = empty_variables()
    for (chemical, coefficient) in zip(self.chemicals, self.chemical_coefficients):
      derivatives[chemical] = coefficient * change
    return derivatives

  def __initialize_values(self, y, constants):
    self.temperature = constants[TEMPERATURE]
    self.density = constants[DENSITY]
    self.rate = self.rate_function(self.temperature)
    self.y = y
  
  def __compute_density_product(self):
    return self.density ** (self.num_reactants - 1)

  def __compute_fractions_product(self):
    fractions_product = 1.0
    for (reactant, coefficient) in zip(self.reactants, self.reactant_coefficients):
      fractions_product *= self.y[reactant] ** coefficient
    return fractions_product

  def get_jacobian(self, y, constants):
    self.__initialize_values(y, constants)
    jacobian = empty_jacobian()
    for (chemical, chemical_coefficient) in zip(self.chemicals, self.chemical_coefficients):
      for reactant_number in range(len(self.reactants)):
        reactant = self.reactants[reactant_number]
        term = self.jacobian_term(reactant_number)
        jacobian[chemical, reactant] = chemical_coefficient * term
    return jacobian

  def jacobian_term(self, reactant_number):
    jacobian_term = 1.0
    (reactants_info, power_coefficient) = self.__compute_power_rule(reactant_number)
    jacobian_term *= power_coefficient
    for reactant, reactant_coefficient in reactants_info:
      jacobian_term *= self.y[reactant] ** reactant_coefficient
    jacobian_term *= self.rate
    jacobian_term *= self.density ** (self.num_reactants - 1)
    return jacobian_term
    
  def __compute_power_rule(self, reactant_number):
    reactants_info = zip(copy.copy(self.reactants), copy.copy(self.reactant_coefficients))
    del reactants_info[reactant_number]
    reactant = self.reactants[reactant_number]
    power_coefficient = self.reactant_coefficients[reactant_number]
    if power_coefficient > 1:
      effective_reactant_coefficient = power_coefficient - 1
      reactants_info.append( (reactant, effective_reactant_coefficient) )
    return (reactants_info, power_coefficient)
