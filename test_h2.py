from fixed_solver import FixedSolver
import numpy as np
import matplotlib.pyplot as pl
from initial_state import InitialState
from reaction_network import ReactionNetwork

# reaction_network = ReactionNetwork('glover_reactions.json')

initial_temperature = 1000.0
initial_density = 1.0
initial_abundances = {
    'H+': 1.0,
    'e': 1.0,
    'H': 1.0,
    'H-': 1.0,
    'H2': 1.0,
    'H2+': 1.0
}

solver = FixedSolver('glover_reactions.json')
  
solver.add_variable("H+", {})
solver.add_variable("e", {})
solver.add_variable("H", {})
solver.add_variable("H-", {})
solver.add_variable("H2", {})
solver.add_variable("H2+", {})

# time
time = np.logspace(12, 22, 1000)

# temperature
initial_temperature = 10000

# initial_state = InitialState(reaction_network, initial_temperature, initial_density, initial_abundances)
initial_state = {
    "temperature": initial_temperature,
    "density": initial_density,
    "initial_abundances": initial_abundances
}
y = solver.solve(time, initial_state)
  
pl.figure()
pl.loglog(time, y['H+'], label="H+ fraction")
pl.loglog(time, y['H'], label="H fraction")
pl.legend()
pl.xlabel('Time (t)')
pl.show()
