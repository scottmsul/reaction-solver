
import math
chemicals = [u'H+', u'e', u'H']
tracked_species = u'proton'
tracked_compositions = {
  u'H+': 1,
  u'e': 0,
  u'H': 1
}

def rate0(T):
  return (((6.28e-11 * (T ** (-0.5))) * ((T / 1000.0) ** (-0.2))) * ((1 + ((T / 1000000.0) ** 0.7)) ** (-1)))

def rate1(T):
  return (((5.85e-11 * (T ** 0.5)) * ((1 + ((T / 100000.0) ** 0.5)) ** (-1))) * math.exp(((-157800.0) / T)))
reactions = [{
  'rate_function': rate0,
  'reactants': {
    u'H+': 1,
    u'e': 1
  },
  'products': {
    u'H': 1
  },
  'chemicals': {
    u'H+': (-1),
    u'e': (-1),
    u'H': 1
  },
  'diagram': u'H+ + e -> H'
}, {
  'rate_function': rate1,
  'reactants': {
    u'H': 1,
    u'e': 1
  },
  'products': {
    u'H+': 1,
    u'e': 2
  },
  'chemicals': {
    u'H': (-1),
    u'e': 1,
    u'H+': 1
  },
  'diagram': u'H + e -> H+ + 2e'
}]