import re

# TODO:
# - make it cleaner
# - check validity of reaction chemical names against known chemical names

class ReactionParser:

  def parse_reactions(self, reactions):
    self.id_count = 0
    return [self.__parse_reaction(reaction) for reaction in reactions]

  def __parse_reaction(self, reaction):
    diagram = reaction['diagram']
    rate = reaction['rate']
    reaction = self.parse_diagram(diagram)
    reaction['rate'] = rate
    reaction['diagram'] = diagram
    reaction['id'] = str(self.id_count)
    reaction['rate_function'] = "rate" + reaction['id']
    self.id_count += 1

    return reaction

  def parse_diagram(self, diagram):
    conjoined_reactants, conjoined_products = self.separate_reactants_from_products(diagram)

    unparsed_reactants = self.split_chemistry_terms(conjoined_reactants)
    unparsed_products = self.split_chemistry_terms(conjoined_products)
  
    reactants = self.parse_chemistry_terms(unparsed_reactants)
    products = self.parse_chemistry_terms(unparsed_products)
    chemicals = self.generate_chemical_coefficients(reactants, products)
  
    reaction = {}
    reaction['reactants'] = reactants
    reaction['products'] = products
    reaction['chemicals'] = chemicals
#     print "REACTANTS: " + str(reactants)
#     print "PRODUCTS: " + str(products)
#     print "CHEMICALS: " + str(chemicals)
  
    return reaction

  def separate_reactants_from_products(self, diagram):
    reactants_and_products = diagram.split(" -> ")
    if(len(reactants_and_products) != 2):
      raise BadFormatError("Diagram \"" + diagram + "\" must consist of exactly one '->' separated by spaces")
    conjoined_reactants = reactants_and_products[0]
    conjoined_products = reactants_and_products[1]
    return (conjoined_reactants, conjoined_products)

  def split_chemistry_terms(self, conjoined_chemistry_terms):
    split_chemistry_terms = conjoined_chemistry_terms.split(" + ")
    for chemistry_term in split_chemistry_terms:
      if ' ' in chemistry_term:
        raise BadFormatError("Diagram \"" + diagram + "\" did not parse correctly. Make sure there is one space between each \"+\" and \"->\", with no other spaces.")
    return split_chemistry_terms

  def parse_chemistry_terms(self, unparsed_chemistry_terms):
    parsed_chemistry_terms = {}
    for chemistry_term in unparsed_chemistry_terms:
      chemical_name, chemical_coefficient = self.parse_chemical(chemistry_term)
      self.insert_chemical(parsed_chemistry_terms, chemical_name, chemical_coefficient)
    return parsed_chemistry_terms

  def parse_chemical(self, chemical):
    chemistry_term_regex = '([0-9]*)(.+)'
    matches = re.match(chemistry_term_regex, chemical)

    coefficient_string = matches.group(1)
    if coefficient_string == '':
      coefficient = 1
    else:
      coefficient = int(coefficient_string)

    chemical_name = matches.group(2)
  
    return (chemical_name, coefficient)

  def generate_chemical_coefficients(self, reactants, products):
    chemicals = {}
    for reactant_name, reactant_coefficient in reactants.iteritems():
      # note the minus sign on the coefficient
      self.insert_chemical(chemicals, reactant_name, -reactant_coefficient)
    for product_name, product_coefficient in products.iteritems():
      # note the coefficient is left positive
      self.insert_chemical(chemicals, product_name, product_coefficient)
    return chemicals
  
  def insert_chemical(self, curr_dict, name, coefficient):
    if name in curr_dict:
      curr_dict[name] += coefficient
    else:
      curr_dict[name] = coefficient
