from ast import *
from unparse import Unparser

def get_stmt_from_code(code):
  module = parse(code)
  stmt = module.body[0]
  return stmt

def get_expr_from_code(code):
  module = parse(code)
  expr = module.body[0].value
  return expr

def get_assign_stmt(var_name, expr):
  var_name_expr = Name(var_name, Store())
  assign_stmt = Assign([var_name_expr], expr)
  return assign_stmt

class ModuleBuilder:

  def __init__(self):
    self.ast = Module([])

  def save_code(self, filename):
    f = open(filename, 'w+')
    Unparser(self.ast, f)
    f.close()

  def output_code(self):
    Unparser(self.ast)
    print ""

  def append_lines_of_code(self, lines_of_code):
    for line in lines_of_code:
      self.append_line_of_code(line)

  def append_line_of_code(self, line):
    stmt = get_stmt_from_code(line)
    self.append_stmt(stmt)

  def assign_string(self, var_name, python_string):
    string_expr = Str(python_string)
    self.assign_expr(var_name, string_expr)

  def assign_list(self, var_name, python_list):
    list_expr = get_expr_from_code(str(python_list))
    self.assign_expr(var_name, list_expr)

  def assign_dict(self, var_name, python_dict):
    dict_expr = get_expr_from_code(str(python_dict))
    self.assign_expr(var_name, dict_expr)

  def assign_expr(self, var_name, expr):
    assign_stmt = get_assign_stmt(var_name, expr)
    self.append_stmt(assign_stmt)

  def define_function(self, function_builder):
    function_stmt = function_builder.function_stmt
    self.append_stmt(function_stmt)

  def append_stmt(self, stmt):
    self.ast.body.append(stmt)

class FunctionBuilder:

  def __init__(self, function_name):
    # we will exploit the fact that these are aliased in the function node
    self.args_list = []
    self.arguments = arguments(self.args_list, None, None, [])
    self.body = []
    self.decorators = []
    self.function_stmt = FunctionDef(function_name, self.arguments, self.body, self.decorators)

  # for now we don't allow default arguments, keyword arguments, or arbitrary-length arguments
  def add_argument(self, var_name):
    param_node = Name(var_name, Param())
    self.args_list.append(param_node)
  
  def append_lines_of_code(self, code):
    body = parse(code).body
    self.body.append(body)

  def append_line_of_code(self, line):
    stmt = get_stmt_from_code(line)
    self.append_stmt(stmt)

  def append_return_expr(self, code):
    expr = get_expr_from_code(code)
    return_stmt = Return(expr)
    self.append_stmt(return_stmt)

  def append_stmt(self, stmt):
    self.body.append(stmt)

class DictionaryBuilder:

  def __init__(self):
    # exploit the fact that these are aliased in the dict node
    self.keys = []
    self.values = []
    self.dict_expr = Dict(self.keys, self.values)

  def insert_id(self, key, value):
    key_expr = Str(key)
    value_expr = Name(value, Load())
    self.insert_expr(key_expr, value_expr)

  def insert_string(self, key, value):
    key_expr = Str(key)
    value_expr = Str(value)
    self.insert_expr(key_expr, value_expr)

  def insert_dict(self, key, value):
    key_expr = Str(key)
    value_expr = get_expr_from_code(str(value))
    self.insert_expr(key_expr, value_expr)

  def insert_expr(self, key_expr, value_expr):
    self.keys.append(key_expr)
    self.values.append(value_expr)

class ListBuilder:

  def __init__(self):
    # items is aliased in the list node
    self.items = []
    self.list_expr = List(self.items, Load())

  def append_dict(self, dict_expr):
    self.append_expr(dict_expr)

  def append_expr(self, expr):
    self.items.append(expr)
