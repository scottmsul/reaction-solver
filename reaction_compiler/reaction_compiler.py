import json
import copy
from reaction_parser import ReactionParser
from ast import *
from unparse import Unparser
from ast_builder import ModuleBuilder, FunctionBuilder, ListBuilder, DictionaryBuilder

class ReactionCompiler:

  def __init__(self, json_filename):
    json_data = self.__get_json_data(json_filename)
    self.__initialize_json_data(json_data)
    self.__initialize_ast()
  
  def __get_json_data(self, json_filename):
    json_file = open(json_filename, "r")
    json_data = json.load(json_file)
    json_file.close()
    return json_data

  def __initialize_json_data(self, json_data):
    self.__initialize_preamble(json_data)
    self.__initialize_chemicals(json_data)
    self.__initialize_tracked_species(json_data)
    self.__initialize_tracked_compositions(json_data)
    self.__initialize_reactions(json_data)
  
  # list of strings (python statements)
  def __initialize_preamble(self, json_data):
    preamble = json_data["preamble"]
    self.preamble = copy.copy(preamble)

  # list of strings (chemical names)
  def __initialize_chemicals(self, json_data):
    chemicals = json_data["chemicals"]
    self.chemicals = copy.copy(chemicals)

  # string (name of the tracked species, eg "proton")
  def __initialize_tracked_species(self, json_data):
    tracked_species = json_data["tracked_species"]
    self.tracked_species = tracked_species

  # dictionary (maps chemical names to their content of the tracked species)
  def __initialize_tracked_compositions(self, json_data):
    tracked_compositions = json_data["tracked_compositions"]
    self.tracked_compositions = copy.copy(tracked_compositions)

  # list of reactions, each reaction is a dictionary
  def __initialize_reactions(self, json_data):
    reaction_parser = ReactionParser()
    reactions = json_data["reactions"]
    parsed_reactions = reaction_parser.parse_reactions(reactions)
    self.__initialize_rate_functions(parsed_reactions)
    self.__initialize_reactions_chemical_data(parsed_reactions)
    self.reactions = parsed_reactions

  def __initialize_rate_functions(self, parsed_reactions):
    rate_functions = []
    for reaction in parsed_reactions:
      rate_function = {}
      rate_function["function_name"] = reaction["rate_function"]
      rate_function["function_body"] = reaction["rate"]
      rate_functions.append(rate_function)
    self.rate_functions = rate_functions

  def __initialize_reactions_chemical_data(self, parsed_reactions):
    reactions_chemical_data = copy.deepcopy(parsed_reactions)
    for reaction in reactions_chemical_data:
      del reaction["rate"]
      del reaction["id"]
    self.reactions_chemical_data = reactions_chemical_data

  def __initialize_ast(self):
    self.module_builder = ModuleBuilder()
    self.module_builder.append_lines_of_code(self.preamble)
    self.module_builder.assign_list("chemicals", self.chemicals)
    self.module_builder.assign_string("tracked_species", self.tracked_species)
    self.module_builder.assign_dict("tracked_compositions", self.tracked_compositions)
    self.__build_rate_functions()
    self.__build_reactions()
  
  def __build_rate_functions(self):
    for rate_function in self.rate_functions:
      self.__build_rate_function(rate_function)

  def __build_rate_function(self, rate_function):
    function_name = rate_function["function_name"]
    function_body = rate_function["function_body"]

    function_builder = FunctionBuilder(function_name)
    function_builder.add_argument("T")
    if(isinstance(function_body, list)):
      function_builder.append_lines_of_code("\n".join(function_body))
    else:
      function_builder.append_return_expr(function_body)

    self.module_builder.define_function(function_builder)

  def __build_reactions(self):
    list_builder = ListBuilder()
    for reaction in self.reactions:
      dictionary_builder = self.__build_reaction(reaction)
      list_builder.append_expr(dictionary_builder.dict_expr)
    self.module_builder.assign_expr("reactions", list_builder.list_expr)

  def __build_reaction(self, reaction):
    dictionary_builder = DictionaryBuilder()
    dictionary_builder.insert_id("rate_function", reaction["rate_function"])
    dictionary_builder.insert_dict("reactants", reaction["reactants"])
    dictionary_builder.insert_dict("products", reaction["products"])
    dictionary_builder.insert_dict("chemicals", reaction["chemicals"])
    dictionary_builder.insert_string("diagram", reaction["diagram"])
    return dictionary_builder

  def save_code(self, filename):
    self.module_builder.save_code(filename)

  def output_code(self):
    self.module_builder.output_code()

def main():
  import sys
  json_filename = sys.argv[1]
  output_filename = "reactions.py"

  reaction_compiler = ReactionCompiler(json_filename)
  #reaction_compiler.output_code(output_filename)
  reaction_compiler.output_code()

if __name__ == "__main__":
  main()
