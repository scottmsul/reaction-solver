from solver import Solver
import numpy as np
import matplotlib.pyplot as pl
from initial_state import InitialState
from reaction_network import ReactionNetwork
from reaction_process import ReactionProcess
from reactions import *
from variables import *

reaction_network = ReactionNetwork('reactions.json')

initial_temperature = 10000.0
initial_density = 1.0
initial_abundances = {
    'H+': 1.0,
    'e': 1.0,
    'H': 1.0
}
initial_fractions = reaction_network.get_initial_fractions(initial_abundances)

differential_processes = []
for reaction in reactions:
  reaction_process = ReactionProcess(reaction)
  differential_processes.append(reaction_process)

solver = Solver(differential_processes)

time = np.logspace(8, 18, 1000)

y0 = empty_variables()
for chemical, value in initial_fractions.iteritems():
  index = CHEMICAL_INDEX_MAP[chemical]
  y0[index] = value

constants = empty_constants()
constants[TEMPERATURE] = initial_temperature
constants[DENSITY] = initial_density

y = solver.solve(time, y0, constants)

h_plus = y[:, IONIZED_HYDROGEN]
h = y[:, HYDROGEN]
e = y[:, ELECTRON]

pl.figure()
pl.loglog(time, h_plus, label="H+ fraction")
pl.loglog(time, h, label="H fraction")
# pl.loglog(time, e, label="e- fraction")
pl.legend()
pl.xlabel('Time (s)')
pl.title('Temperature = 10000k')
pl.show()
