from solver import Solver
import numpy as np
import matplotlib.pyplot as pl

solver = Solver('glover_reactions.json')

# time
time = np.logspace(6, 25, 1000)

# temperature
temperature = np.logspace(4, 5, 100)

# final H,H+ concentration
h = np.zeros(100)
h_plus = np.zeros(100)

solver.set_initial_temperature(10000)
solver.set_initial_abundances(
  {
    "H": 1.0,
    "H+": 1e-6,
    "e": 1e-6,
    "H2": 1e-6,
  }
)
y = solver.solve(time)
h_plus = y['H+']
h = y['H']

pl.figure()
pl.loglog(time, h_plus, label="H+ fraction")
pl.loglog(time, h, label="H fraction")
pl.loglog(time, y['H2'], label="H2 fraction")
pl.loglog(time, y['e'], label="electron fraction")
pl.legend()
pl.xlabel('Time (s)')
pl.show()
