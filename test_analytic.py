from fixed_solver import FixedSolver
import numpy as np
import matplotlib.pyplot as pl

time = np.logspace(-4, 6, 1000);

initial_temperature = 57.3
initial_density = 99.22

initial_abundances = {
  "A": 1.0,
  "B": 0.0
}

initial_state = {
    "temperature": initial_temperature,
    "density": initial_density,
    "initial_abundances": initial_abundances
}
solver = FixedSolver('test_analytic.json')
solver.add_variable("A", {})
solver.add_variable("B", {})
y = solver.solve(time, initial_state)

tau = 1 / (1.772e-8 * initial_temperature ** 2)
A = np.exp(-time / tau)
B = 1 - A

pl.figure()
pl.loglog(time, A, label="A fraction (analytic)")
pl.loglog(time, y['A'], label="A fraction (numeric)")
pl.loglog(time, B, label="B fraction (analytic)")
pl.loglog(time, y['B'], label="B fraction (numeric)")
pl.legend(bbox_to_anchor=(1.0, 1.0))
pl.xlabel('Time')
pl.ylabel('Fractional Abundance')
pl.title(r'$A \rightarrow B$')
pl.show()
