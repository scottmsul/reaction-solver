from python_types import *

test_int = PythonNumber(4)
print get_code_text(test_int.get_code())

test_string = PythonString("Hello world!")
print get_code_text(test_string.get_code())

test_list = PythonList([
  PythonNumber(1),
  PythonString("second element"),
  PythonList([
    PythonNumber(20),
    PythonNumber(21),
    PythonNumber(22)
  ]),
  PythonNumber(40)
])

print get_code_text(test_list.get_code())

test_identifier = PythonIdentifier("x")
print get_code_text(test_identifier.get_code())

test_assign_string = PythonAssignment(test_identifier, test_string)
print get_code_text(test_assign_string.get_code())

test_assign_list = PythonAssignment(test_identifier, test_list)
print get_code_text(test_assign_list.get_code())
