from solver import Solver
import numpy as np
import matplotlib.pyplot as pl
from reaction_network import ReactionNetwork
from processes.fixed_reaction_process import FixedReactionProcess
from reactions import *
from variables import *

reaction_network = ReactionNetwork('chemical_networks/reactions.json')

initial_temperature = 1000.0
initial_density = 1.0
initial_abundances = {
    'H+': 1.0,
    'e': 1.0,
    'H': 1.0
}
initial_fractions = reaction_network.get_initial_fractions(initial_abundances)

differential_processes = []
for reaction in reactions:
  reaction_process = FixedReactionProcess(reaction)
  differential_processes.append(reaction_process)


solver = Solver(differential_processes)

time = np.logspace(12, 22, 1000)
temperature = np.logspace(4, 5, 30)
e = np.zeros(30)
h = np.zeros(30)
h_plus = np.zeros(30)

for i in range(len(temperature)):
  initial_temperature = temperature[i]

  y0 = empty_variables()
  for chemical, value in initial_fractions.iteritems():
    index = CHEMICAL_INDEX_MAP[chemical]
    y0[index] = value

  constants = empty_constants()
  constants[TEMPERATURE] = initial_temperature
  constants[DENSITY] = initial_density

  y = solver.solve(time, y0, constants)
  curr_h_plus = y[:, IONIZED_HYDROGEN]
  curr_h = y[:, HYDROGEN]
  curr_e = y[:, ELECTRON]
  h_plus[i] = curr_h_plus[-1]
  h[i] = curr_h[-1]
  e[i] = curr_e[-1]

  print i

pl.figure()
pl.loglog(temperature, h_plus, label="H+ fraction")
pl.loglog(temperature, h, label="H fraction")
pl.loglog(temperature, e, label="e- fraction")
pl.legend()
pl.xlabel('Temperature (K)')
pl.title('with Jacobian')
pl.show()
