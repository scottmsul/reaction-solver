import numpy as np

# variables
IONIZED_HYDROGEN = 0
HYDROGEN = 1
ELECTRON = 2
NUM_VARIABLES = 3

# constants
TEMPERATURE = 0
DENSITY = 1
NUM_CONSTANTS = 2

CHEMICAL_INDEX_MAP = {
    "H+": IONIZED_HYDROGEN,
    "e": ELECTRON,
    "H": HYDROGEN
}

def empty_variables():
  return np.zeros(NUM_VARIABLES)

def empty_jacobian():
  return np.zeros( (NUM_VARIABLES, NUM_VARIABLES) )

def empty_constants():
  return np.zeros(NUM_CONSTANTS)
