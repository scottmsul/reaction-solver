from unparse import Unparser
from ast import *

dict1 = {"a": 1, "b": 2, "c": 3}
tree = parse(str(dict1))
Unparser(tree)

dict2 = {"a": 1, "b": dict1, "c": 3}
tree = parse(str(dict2))
Unparser(tree)

list1 = ["a", "b", "c"]
tree = parse(str(list1))
Unparser(tree)

dict3 = {"a": 1, "b": dict2, "c": list1}
tree = parse(str(dict3))
Unparser(tree)
