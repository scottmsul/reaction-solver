from ast import *
from ast_printer import *

int_node = Num(4)
python_printer = create_python_printer(int_node)
print python_printer.get_code()

str_node = Str("hello")
python_printer = create_python_printer(str_node)
print python_printer.get_code()


node1 = Num(1)
node2 = Num(2)
node3 = Num(3)
node4 = Num(4)
list_node = List([node1, node2, node3, node4], Load())
python_printer = create_python_printer(list_node)
print python_printer.get_code()

nested_list_node = List([node1, node2, list_node, node4], Load())
python_printer = create_python_printer(nested_list_node)
print python_printer.get_code()

dict_keys = [Str("a"), Str("b"), Str("c")]
dict_values = [node1, node2, node3]
dict_node = Dict(dict_keys, dict_values)
python_printer = create_python_printer(dict_node)
print python_printer.get_code()

nested_dict_keys = [Str("a"), Str("b"), Str("c")]
nested_dict_values = [list_node, dict_node, nested_list_node]
nested_dict_node = Dict(nested_dict_keys, nested_dict_values)
python_printer = create_python_printer(nested_dict_node)
print python_printer.get_code()

name_node = Name("x", Store())
python_printer = create_python_printer(name_node)
print python_printer.get_code()

assign_single_node = Assign([name_node], list_node)
python_printer = create_python_printer(assign_single_node)
print python_printer.get_code()

x_node = Name("x", Store())
y_node = Name("y", Store())
z_node = Name("z", Store())

assign_multiple_node = Assign([x_node, y_node, z_node], list_node)
python_printer = create_python_printer(assign_multiple_node)
print python_printer.get_code()
