import importlib
import copy
from reaction_compiler.reaction_compiler import ReactionCompiler

class ReactionNetwork:

  def __init__(self, json_file):
    reaction_compiler = ReactionCompiler(json_file)
    reaction_compiler.save_code('reactions.py')
    reactions_import = importlib.import_module('reactions')

    self.chemicals = reactions_import.chemicals
    self.tracked_species = reactions_import.tracked_species
    self.tracked_compositions = reactions_import.tracked_compositions
    self.reactions = reactions_import.reactions


  def get_chemicals(self):
    return copy.copy(self.chemicals)
  
  def get_tracked_species(self):
    return self.tracked_species

  def get_tracked_compositions(self):
    return copy.deepcopy(self.tracked_compositions)

  def get_reactions(self):
    return copy.deepcopy(self.reactions)
  
  def get_initial_fractions(self, initial_abundances):
    initial_fractions = {}
    total_tracked = 0.0
    for chemical_name, contained_tracked_species in self.get_tracked_compositions().iteritems():
      if contained_tracked_species > 0:
        total_tracked += contained_tracked_species * initial_abundances[chemical_name]
    for chemical_name in self.get_chemicals():
      initial_fractions[chemical_name] = initial_abundances[chemical_name] / total_tracked
    return initial_fractions
